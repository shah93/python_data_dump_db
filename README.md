# Project Title
### Python_Data_Dump_DB

# Description
### Python Script to Dump Bulk Data from SQL DB

# Prerequisites
1. Python 2.7
2. Python 'mysql-connector-python' Library

# Installing
1. Install python in your system
2. Download mysql-connector-python library using pip [run 'pip install mysql-connector-python' from python console]
3. Run the File from python/windows command line

# Output

### Dumps the bulk data from DB into destined path with specified file type extension [JSON as an example]
